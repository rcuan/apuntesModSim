# Simulación

## Introducción

### Objetivo General

1. Diseñarán modelos de simulación de sistemas
2. de Naturaleza Estocástica
3. Simulación Montecarlos
4. Apoyo estadístico
5. Se trabajará con Excel.

### Conceptos a tener en cuenta

1. Ciencia
2. Tecnología
3. Investigación
4. Innovación
5. Ingeniería
6. Ingeniería en Sistemas
7. Simulación

Sincronicemos algunos conceptos

Ciencia: 
1. f. Conjunto de conocimientos obtenidos mediante la observación y el razonamiento, sistemáticamente estructurados y de los que se deducen principios y leyes generales concapacidad predictiva y comprobables experimentalmente.

From <https://dle.rae.es/ciencia> 

Tecnología: 
1. f. Conjunto de teorías y de técnicas que permiten el aprovechamiento práctico delconocimiento científico.

From <https://dle.rae.es/tecnolog%C3%ADa> 

Investigacion: 
investigación básica
1. f. investigación que tiene por fin ampliar el conocimiento científico, sin perseguir, enprincipio, ninguna aplicación práctica.

From <https://dle.rae.es/investigaci%C3%B3n> 
Innovación: 
2. f. Creación o modificación de un producto, y su introducción en un mercado.

From <https://dle.rae.es/innovaci%C3%B3n> 

Ingenieria: 
Gral. 
Conjunto de conocimientos y técnicas científicas y empíricas aplicadas a la invención, el diseño, el desarrollo, la construcción, el mantenimiento y el perfeccionamiento de tecnologías, estructuras, máquinas, herramientas, sistemas, materiales y procesos para la resolución de problemas prácticos. 

Ingenieria de sistemas: 
La ingeniería de sistemas es una rama interdisciplinaria de la ingeniería que permite estudiar y comprender la realidad, con el propósito de implementar u optimizar sistemas complejos. 

From <https://es.wikipedia.org/wiki/Ingenier%C3%ADa_de_sistemas> 


Simulacion: 
2. f. Der. Alteración aparente de la causa, la índole o el objeto verdadero de un acto ocontrato.

From <https://dle.rae.es/simulaci%C3%B3n> 

La simulación es el artificio contextual que hace referencia a la investigación de una hipótesis o un conjunto de hipótesis de trabajo utilizando modelos un método perfecto para la enseñanza y aprendizaje.

From <https://es.wikipedia.org/wiki/Simulaci%C3%B3n>

```mermaid
graph LR
  SistemaReal--Conjunto de asunciones con respecto al sistema-->id(ModeladoYAnálisis)
```

#### Simulación

1. La imitación de la operación de un sistema o proceso de la vida real a través del tiempo
2. Desarrollo de un conjunto de asunciones de relaciones matemáticas, lógicas y simbólicas entre las entidades de interés de un sistema
3. Estimación de la medida de desempeño de un sistema con los datos generados por medio de la simulación

#### El modelado puede ser utilizado como:

1. Herramienta de análisis para predecir el efecto de cambios a un sistema existente
2. Herramienta de diseño para predecir el desempeño de un sistema nuevo

### Simuladores que ha hecho el profesor

#### Simulador de Negocios

El profesor creó un laboratorio de simulador de negocios.

Ingeniero financiero tiene una formación mucho mas matemáticas y sistemáticas, incluyendo simulación.

Ahora tiene un valor de 1 millón de dolares.

#### Simulador de un cadáver

Laboratorio de anatomía. Tiene un ipad del tamaño de una persona real. Se comunica en tiempo real con proyector.

Vinieron universidades de todas partes del mundo para ver el simulador.

#### Simulador de equipo pesado

240 000 de dolares.

#### Simulador de vaca

80 000 de dolares. El animal sufre mucho cuando los estudiantes mal ordeñan la vaca.

#### Simulador de producción industrial

#### Simulador de Guerra

#### Mini laboratorio de Robótica

Teoria de carga, espera

#### Simulador del corazón

Simula todos los tipos de latidos del corazón

### Objetivos Específicos

1. Elaborar algoritmos generadores de números aleatorios y de las pruebas para determinar la correcta generación de los mismos en la aplicación de la técnica de Montecarlo
2. Desarrollar la habilidad de representar sistemas discretos a través de un lenguaje de simulación y adquirir capacidad de interpretar dichos modelos.

### Contenido

1. C1 Introducción a la Simulación de Sistemas
2. C2 Definición y Construcción de Modelos
   1. Modelo matemáticos
   2. Puede ser toda una materia
3. C3 Conceptos de Probabilidades
   1. Valencianas
4. C4 Recolección de Datos y Prueba de Hipótesis
5. C5 Generación de Números Aleatorios
6. C6 Simulación de Colas
7. C7 Simulación de Montecarlo
8. C8 Lenguaje de Simulación
9. C9 Lenguaje de Simulación Arena
10. C10 Lenguaje de Simulación ProModel

### Evaluaciones

1. Asistencia y Portafolio 5%
2. Asignaciones 20%
3. 2 Parciales 22%
4. Proyecto Final
5. Examen Semestrales 30%

## C1 Introducción a la Simulación de Sistemas

### El mundo que nos tocó vivir

Economía basada en el conocimiento. El enfoque económico y social nos ha obligado eliminar el conocimiento líquido:

Conocimiento Líquido

1. Desorganzado
2. abundancia
3. deciciones rápidas

Conocimiento sólido:

1. Estable
2. Duradera
3. Organizada
4. Filtrada
5. Monitorizada
6. Producto acabado

"Aunque la buena enseñanza se vale de rutinas, rara vez es rutinaria. Se basa en la sensibilidad y la "

### TIC

Tecnologías de la Información y la Comunicación

Te Interesa Cambiar

Tenemos Intereses Comunes

### Creatividad

El profesor nos demanda creatividad, espera que tengamos un esfuerzo creativo muy grande.

Tratemos de ser inovadores, aceleren el producto final, para que sea digna a lo que requerimos.

Espera tener un High Performance Course, para ser un High Performance Estudent.

### Ludificación y Clase Invertida

Estas clases están enfocadas a que el estudiante tenga mas protagonismo. Clases lúdicas controladas por estudiantes.

### Flupped Classroom

1. Modelo pedagógico
2. Traslada el trabajo de unos aspectos de aprendizaje fuera del aula a casa
3. Utiliza el tiempo de clase para facilitar y potenciar la adquisición y práctica de conocimiento de ntro del aula con la ayuda del docente.

### Reglas de la NORMA IEEE

Es importantísimo de que sepas las reglas. Citas

### Infracciones de derecho de autor

¿Qué es una infracción del derecho de autor?

La infracci´´on ocurre cuando una obra protegida por el derecho de autor es utilizada (reproducida, traducida, adaptada, exhibida o interpetada en público, distribuida, emitida o comunicada al público) sin el permiso de los títulos de los derechos y dicho uso no está cubierto por ninguno de los limites al derecho de autor.

## Capitulo 1: Introducción a la Simulación

1. Presentar los conceptos más importantes de la simulación
2. Identificar y especificar las clases de sistemas, sus características y comportamiento
3. Determinar los usos, campos de aplicación , ventajas y desventajas de la simulación, sus diferenctes tipos y técnicas para poder entender mejor su aplicación den el estudio

### 1.1 Concepto de Sistemas

Un sistema es "un conjunto o conglomerado de entidades (caracterizadas por una serie de atributos) relacionadas y debidamente estructuradas por alguna interacción estructuradas por alguna interacción o interdependencia regular, que operan mancomundamante mediante una red de comunicación y que tienen el propósito de lograr algún objetivo"

¿Es este un sistema?

Objetivo: Transitar por la Ciudad de Panamá

No, si el vehículo lo tiene que controlar un humano, pero SÍ si el vehículo es autónomo

Objetivo: Procesar Instrucciones

Lamentablemente no existe acuerdo respecto a una definición precisa de la palabra simulación y la propuesta por C. West Churchman es estrictamente formal

"X simula a Y" si y solo si

Un sistema dinámico se define formalmente como:

1. Un espacio de estado X,
2. Un conjunto de tiempo T
3. Una regla R que especifíca cómo evoluciona el estado (sistema) con el tiempo

La regla R es una función cuyo dominio es X x T y cuyo codominio es X, es decir, R: X x T -> X

La función de regla R significa que R toma dos entradas, R = R (x,t), donde:

* X c X es el estado inicial (en el tiempo t=0, por ejemplo)
* T c T es un tiempo futuro

En otras palabras, R(x,t) da el estado en el tiempo t dado que el estado inicial era x

Martin Shubik

### Tipos de simulacion veremos en esta signuatura

Continuo y Discreto

Imaginemos que tenemos dos sistemas (1 continuo y el otro discreto) que se inicializan y que, dentro de ellos, se llevan a cabo algunos procesos. Los eventos asociados con estos procesos se muestran en sus respectivas líneas de tiempo en verde.

Como podemos ver, en la simulación continua los eventos ocurren a intervalos regulares, mientras que en la simulación de ventos discretos los ventos ocurren a intervalos irregulares.

También es posible que ocurran múltiples eventos exactamente al mismo tiempo en la simulación de eventos discretos (mostrados por los eventos apilados en verde). Los eventos que ocurren al mismo tiempo pueden procesarse en orden de llegada.

Todos los evenetos en simulaciones continuas se procesan al mismo tiempo.

Ahora consideremos la llegada de nuevas entidades (clientes) a cada sistemas, que se muestra en azul.

En la simulació de eventos discretos, estos eventos proceden cuando ocurren. En la simulación continua, es probable que estos eventos estén almacenados temporalmente y que el procesamiento se realice.

Lo mismo ocurre con lo eventos de servidores en rojo.

Los siguientes comentarios se aplican a ambos tipos de simulaciones, excepto donde se indique.

En una simulación continua, el reloj de simulación siempre avanza una cantidad fija, el reloj de simulación avanza tanto como sea necesario para llegar al siguiente evento.

Si se producen múltiples eventos en una simulación de eventos discreto al mismo tiempo simulando un reloj.

En tiempo de computadora, el evento o eeventos pueden procesarse lo más rápido posible, el reloj puede avanzar y el siguiente evento o eventos pueden procesarse.

Si el tiempo simulado se mueve más rápido que el tiempo en el mundo real (debido a que la simulación es menos compleja, implica menos o más tiempo), entonces es posible insertar eventos.

La escala de tiempo de cualquier tipo de simulación puede variar temporalmente.

La simulación de partícula subatómica pueden estar en escalas de tiempo casi infinitesimales, mientras que las simulaciones geológicas puede tardar mas.

Las simulaciones de actividades a escala humana, como procesos de fabricación, evacuaciones de edificioes.

Algunas simulaciones tardan horas o días en ejecutarse, incluso en supercomputadoras. Otros pueden correr casi instantaneamente.

El avance del tiempo está predefinidos, ya que los eventos ocurren en ciertos momentos.

Poisson para eventos externo vs Exponencial para eventos interno
